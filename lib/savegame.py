#!/usr/bin/env python

import logging
import os
import sys
from binascii import hexlify, unhexlify
from datetime import datetime


class SaveGame(object):

    def __init__(self, file_name):
        self.file_name = file_name
        self.map = None
        self.players = []
        print(self.file_name)
        self.save_date = datetime.fromtimestamp(os.path.getmtime(self.file_name))
        print("date: {0}".format(self.save_date))
        with open(self.file_name, "rb") as fh:
            # Wenn man bei offset 0x10 beginnt, funktioniert
            # die Entschluesselung des folgenden Abschnitts.
            # Innerhalb des Abschnitts ab offset 24 beginnt der mapname
            offset_map_read = 0x10
            fh.seek(offset_map_read)
            map = self.read_fixed_string(fh, 0x100)
            map = self.decode_string(string=map)
            map = map[24:]
            real_map = []
            for i in xrange(100, 900, 1):
                if i % 5 == 0:
                    real_map.append(map[i])
            map = "".join(real_map)
            if map.find(".map") >= 0:
                map = map.split(".map")[0]
                map = "{0}.map".format(map)
            elif map.find("Random") >= 0:
                map = map.split("Random")[0]
                map = "{0}Random".format(map)
            else:
                map = self.get_zero_string(string=map)
            self.map = map
            print("map: {0}".format(self.map))
            for x in xrange(20):
                off = 0x392 + x * 0x134
                fh.seek(off)
                string1 = self.read_fixed_string(fh, length=100)
                string2 = self.decode_string(string1)
                player = self.get_zero_string(string2)
                print("offset:\t{0},\tname: {1}".format(off, player))
#             test = 0
#             while test < 914:
#                 fh.seek(test)
#                 string1 = fh.read(0x200)
#                 string2 = self.decode_string(string1)
#                 string = []
#                 for x in xrange(len(string2) - 1):
#                     if (x + 1) % 5 == 0:
#                         string.append(string2[x])
#                 print("".join(string))
#                 print("\n\n")
#                 test += 1
#             fh.seek(0x10)
#             string1 = fh.read(0x100000)
#             string2 = self.decode_string(string1)
#             with open("{0}_decoded_save.txt".format(os.path.basename(file_name)), "w") as fhh:
#                 for x in xrange(len(string2) - 1):
#                     if (x + 1) % 5 == 0:
#                         fhh.write(string2[x])

    def decode_string(self, string):
        """
        * versucht die Verschluesselung der S3 savegames rueckgaengig zu
        * machen. Der String input muss mit einem Zeichen beginnen,
        * bei dem der verschluesselte Wert gleich dem entschluesselten
        * Wert ist. Wo solche Starts von Verschluesselungssequenzen sind,
        * laesst sich nicht definitiv sagen, haeufig steht zumindest links
        * davon mindestens eine '0'.
        *
        * Aus dem aktuellen Zeichen und dessen Verschluesselung wird
        * eine XOR-mask fuer das folgende Zeichen berechnet.
        * Fuer das erste Zeichen kennt man diese XOR-mask nicht, daher
        * wird sie auf 0 gesetzt, was zur Folge hat, dass Ver- und Entschluesselung
        * vom ersten Zeichen identisch sind (bzw. sein muessen).
        * Fuer die Spielernamen in den S3-savgames ist dieses tatsaechlich der Fall,
        * der erste Buchstabe steht immer im Klartext drin.
        *
        * So wird die XorMask fuer das folgende Zeichen aus dem aktuellen Zeichen berechnet:
        * plainValueXorMask = (2 * unverschluesselter Wert) XOR unverschluesselter Wert
        *
        * xorMask = (2 * verschluesselter Wert) XOR plainValueXorMask
        *
        * Mit dieser xorMask kann jetzt das folgende Zeichen entschluesselt werden.
        """
        byte_list = []
        xor_mask = 0
        value = 0
        dec_value = 0
        for i in xrange(len(string)):
            j = i
            value = string[j]
            # print(hexlify(value))
            value = int(hexlify(value), 16) & 0xff
            # die XOR-Maske, die aus dem vorherigen Zeichen berechnet wurde
            # entschluesselt das aktuelle Zeichen
            # print(value)
            dec_value = value ^ xor_mask
            dec_value_hex = dec_value & 0xff
            dec_value_hex = hex(dec_value_hex)
            # print(dec_value_hex)
            dec_value_hex = "%010x" % (int(dec_value_hex, 16) & 0xff)
            # print(dec_value_hex)
            dec_value_hex = unhexlify(dec_value_hex)
            # print(dec_value_hex)
            byte_list.append(dec_value_hex)
            # Berechnung der xorMask fuer das folgende Zeichen
            plain_value_xor_mask = 2 * dec_value ^ dec_value & 0xff
            xor_mask = (2 * value) ^ plain_value_xor_mask & 0xff
        result = "".join(byte_list)
        return result

    def get_zero_string(self, string):
        """
        Wenn input einen Null-terminierten String enthaelt (C/C++ Standard),
        wird dieser zurueckgeliefert.
        """
        result = ""
        # print("0: " + string)
        # print("A: " + string[:26])
        hex_string = hexlify(string)
        # hex_string = "%010x" % int(hex_string, 26)
        # print("B: " + hex_string)
        index = hex_string.find("0000000000")
        if index >= 0:
            result = hex_string.split("0000000000")[0]
            # result = "%010x" % int(result, 16)
            # print("C: " + result)
            result = result.replace("00", "")
            result = result.replace("ff", "")
            try:
                result = unhexlify(result)
            except TypeError:
                # print(string)
                # print(result)
                result = unhexlify("{0}0".format(result))
                # raise
        return result

    def read_fixed_string(self, fh, length):
        result = fh.read(length)
        return result


if __name__ == '__main__':
    saves = os.listdir(sys.argv[1])
    for save in saves:
        if save.endswith(".mps"):
            sg = SaveGame(file_name=os.path.join(sys.argv[1], save))
            print("----------------------------------")
